import CarStatus from '../models/CarStatus';
import CarHistory from '../models/CarHistory';

export default class Car {
    private _id: number;
    private _model: string;
    private _carStatus: CarStatus;
    private _carHistory: CarHistory;

    constructor(id: number, model: string, carStatus: CarStatus, carHistory: CarHistory) {
        this._id = id;
        this._model = model;
        this._carStatus = carStatus;
        this._carHistory = carHistory;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get model(): string {
        return this._model;
    }

    set model(value: string) {
        this._model = value;
    }

    get carStatus(): CarStatus {
        return this._carStatus;
    }

    set carStatus(value: CarStatus) {
        this._carStatus = value;
    }

    get carHistory(): CarHistory {
        return this._carHistory;
    }

    set carHistory(value: CarHistory) {
        this._carHistory = value;
    }

    public static from(json: JSON): Car {
        return new Car(json['id'], json['model'],
            CarStatus.from(json['carStatus']), json['carHistory'] == null ? null : CarHistory.from(json['carHistory']));
    }

    public static fromArray(json: Array<JSON>): Array<Car> {
        let cars: Array<Car> = [];
        for (let jsonElement of json) {
            cars.push(this.from(jsonElement))
        }
        return cars;
    }

}