import Car from '../models/Car';
import CarFilter from '../utils/CarFilter';

export default class ParsedCar {

    static modelName:string = "model";
    static models: string[] = ["mazda2", "mazda3", "cx-3", "cx-5", "mazda6", "mx-5", "other"];
    static equipmentName: string = 'equipment';
    static equipments: string[] = ['prime', 'center', 'exclusive', 'sport', 'other'];
    static motorName: string = 'motor';
    static motors: string[] = ['diesel', 'benzin', 'other'];
    static transmissionName: string = 'transmission';
    static transmissions: string[] = ['automatik', 'manual', 'other'];

    car:Car;
    model:string = undefined;
    equipment:string = undefined;
    motor:string = undefined;
    transmission:string = undefined;

    constructor(car: Car) {
        this.car = car;
        this.updateData();
    }

    updateData() {
        let modelFound = false;
        for (let model of ParsedCar.models) {
            if (model != 'other' && CarFilter.carModesConstrain(this.car, model)) {
                modelFound = true;
                this.model = model;
                break;
            }
        }

        if (!modelFound) {
            this.model = "other";
        }

        let equipmentFound = false;
        for (let equipment of ParsedCar.equipments) {
            if (equipment != 'other' && CarFilter.carModesConstrain(this.car, equipment)) {
                equipmentFound = true;
                this.equipment = equipment;
                break;
            }
        }

        if (!equipmentFound) {
            this.equipment = "other";
        }


        let motorFound = false;
        for (let motor of ParsedCar.motors) {
            if (motor != 'other' && CarFilter.carMotorIs(this.car, motor)) {
                motorFound = true;
                this.motor = motor;
                break;
            }
        }

        if (!motorFound) {
            console.log('motor not found');
            console.log(this.car);
            this.motor = 'other';
        }

        let transmissionFound = false;
        for (let transmission of ParsedCar.transmissions) {
            if (transmission != 'other' && CarFilter.carTransmissionIs(this.car, transmission)) {
                transmissionFound = true;
                this.transmission = transmission;
                break;
            }
        }
        if (!transmissionFound) {
            console.log('transmission not found');
            console.log(this.car);
            this.transmission  = 'other';
        }

    }
}