import Car from '../models/Car';

export default class CarStatus {

    private _id: number;
    private _status: string;
    private _cars: Car[];

    constructor(id: number, status: string, cars: Car[]) {
        this._id = id;
        this._status = status;
        this._cars = cars;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get status(): string {
        return this._status;
    }

    set status(value: string) {
        this._status = value;
    }

    get cars(): Car[] {
        return this._cars;
    }

    set cars(value: Car[]) {
        this._cars = value;
    }

    public static from(json: JSON): CarStatus {
        return new CarStatus(json['id'], json['status'], Car.fromArray(json['cars']));
    }

    public static fromArray(json: JSON[]): CarStatus[] {
        const statuses: CarStatus[] = [];

        for (const jsonElement of json) {
            statuses.push(this.from(jsonElement));
        }
        return statuses;
    }

}
