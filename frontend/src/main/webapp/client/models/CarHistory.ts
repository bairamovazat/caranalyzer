import moment from 'moment/moment';

export default class CarHistory{

    private _id:number;
    private _mark:string;
    private _deliveryDate:Date;
    private _carType:number;
    private _price:number;

    constructor(id: number, mark: string, deliveryDate:Date, carType:number, price:number) {
        this._id = id;
        this._mark = mark;
        this._deliveryDate = deliveryDate;
        this._carType = carType;
        this._price = price;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get mark(): string {
        return this._mark;
    }

    set mark(value: string) {
        this._mark = value;
    }

    get deliveryDate(): Date {
        return this._deliveryDate;
    }

    set deliveryDate(value: Date) {
        this._deliveryDate = value;
    }

    get carType(): number {
        return this._carType;
    }

    set carType(value: number) {
        this._carType = value;
    }

    get price(): number {
        return this._price;
    }

    set price(value: number) {
        this._price = value;
    }

    public static from(json:JSON):CarHistory{
        if(json["deliveryDate"] != null && moment(json["deliveryDate"],'YYYY-MM-DD HH:mm:ss').format("YYYY-MM-DD HH:mm:ss") != json["deliveryDate"]){
            console.log(json);
            console.log(moment(json["deliveryDate"],'YYYY-MM-DD HH:mm:ss'))
        }
        let date: Date = json["deliveryDate"] == null ? null
            : moment(json["deliveryDate"],'YYYY-MM-DD HH:mm:ss').toDate();

        return new CarHistory(json["id"], json["mark"], date, json["carType"], json["price"]);
    }

    public static fromArray(json:Array<JSON>):CarHistory[]{
        let carHistories:Array<CarHistory> = [];
        for (let jsonElement of json) {
            carHistories.push(this.from(jsonElement))
        }

        return carHistories;
    }

}