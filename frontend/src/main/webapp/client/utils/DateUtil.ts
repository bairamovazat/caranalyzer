import moment from 'moment/moment';

export default class DateUtil{

    public static formatDate(date:Date, format:string){
        return moment(date.toISOString()).format(format)
    }
}