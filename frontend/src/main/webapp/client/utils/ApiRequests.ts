
export default class ApiRequests {
    public static apiUrl = '.';

    public static statuses: string = ApiRequests.apiUrl + '/statuses';

    public static status(statusId): string {
        return ApiRequests.apiUrl + '/statuses/' + statusId;
    }

    public static statusCars(statusId): string {
        return ApiRequests.apiUrl + '/statuses/' + statusId + '/cars';
    }

    public static statusCarsByCarType(statusId, carType): string {
        return ApiRequests.apiUrl + '/statuses/' + statusId + '/cars?carType=' + carType;
    }

    public static accountBalance(): string {
        return ApiRequests.apiUrl + '/account/balance';
    }
}
