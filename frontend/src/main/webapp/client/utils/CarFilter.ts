import Car from '../models/Car';

export default class CarFilter {

    public static getMazdaCars(cars: Car[]): Car[] {
        return cars.filter(e => e.carHistory != null)
            .filter(e => e.carHistory.mark != null)
            .filter(e => e.carHistory.mark.trim().toLowerCase().includes('mazda'));
    }


    public static carModesConstrain(car: Car, value: string): boolean {
        return car.model.trim().toLowerCase().includes(value.toLowerCase());
    }

    public static carMotorIs(car: Car, property: string): boolean {
        if (property == 'diesel') {
            return this.carModesConstrain(car, 'SKYACTIV-D') || this.carModesConstrain(car, 'mzr-cd');
        } else if (property == 'benzin') {
            return this.carModesConstrain(car, 'SKYACTIV-G') || this.carModesConstrain(car, 'mzr');
        } else {
            return false;
        }
    }

    public static carTransmissionIs(car: Car, property: string): boolean {
        if (property == 'automatik') {
            return this.carModesConstrain(car, '6AG')
                || this.carModesConstrain(car, '7AG')
                || this.carModesConstrain(car, '8AG')
                || this.carModesConstrain(car, '6AT')
                || this.carModesConstrain(car, '7AT')
                || this.carModesConstrain(car, '8AT');
        } else if (property == 'manual') {
            return this.carModesConstrain(car, '5GS')
                || this.carModesConstrain(car, '6GS');
        } else {
            return false;
        }
    }
}