import {Vue} from 'vue-property-decorator';

import VueI18n from 'vue-i18n';
// here
Vue.use(VueI18n);

export default new VueI18n({
    locale: 'de',
    fallbackLocale: 'de',
    messages: {
        ru: {
            nav: {
                allCars: 'Все машины',
                statistic: 'Статистика'
            },
            carPage: {
                viewAllCar: 'Просмотр всех машин',
                viewAllCarDesc: 'Позволяет просмотреть все машины по статусу',
                selectCarStatus: 'Выберите статус машины',
                onlyMazdaMotors: 'Только Mazda Motors'
            },
            carList: {
                model: 'Модель',
                mark: 'Марка',
                price: 'Цена',
                deliveryDate: 'Дата доставки'
            },
            carStatisticPage: {
                description: 'Подробная статистика',
                orderedCarsScore : 'Автомобилей заказано на сумму',
                accountBalance: 'Баланс счёта'
            },
            carStatistic: {
                model: 'Модель',
                state: 'Статус',
                total: 'Всего',
                equipmentprime : 'Prime',
                equipmentcenter: 'Center',
                equipmentexclusive: 'Exclusive',
                equipmentsport: 'Sport',
                equipmentother: 'Другие комплектации',
                motordiesel: 'Дизель',
                motorbenzin: 'Бензин',
                motorother: 'Другой тип двигателя',
                transmissionautomatik: 'Автоматическая КПП',
                transmissionmanual: 'Ручная КПП',
                transmissionother: 'Другой тип КПП',
                price: 'Цена',
                usedCarsOnly: 'Только подержанные автомобили'

            },
            carStatisticElement: {
            },
            loading: 'Загрузка',
        },
        de: {
            nav: {
                allCars: 'Alle Autos',
                statistic: 'MMD Statistik'
            },
            carPage: {
                viewAllCar: 'Alle Fahrzeuge',
                viewAllCarDesc: 'Nach Status Anzeigen',
                selectCarStatus: 'Status auswählen',
                onlyMazdaMotors: 'Nur MMD Fahrzeuge'
            },
            carList: {
                model: 'Model',
                mark: 'Marke',
                price: 'Preis',
                deliveryDate: 'Lieferdatum',
            },
            carStatisticPage: {
                description: 'Detaillierte Statistik',
                orderedCarsScore: 'MMD Vorlauf z Z.',
                accountBalance: '€  Noch Verfügbar'
            },
            carStatistic: {
                model: 'Modell',
                state: 'Status',
                total: 'Total',
                equipmentprime : 'Prime',
                equipmentcenter: 'Center',
                equipmentexclusive: 'Exclusive',
                equipmentsport: 'Sport',
                equipmentother: 'Andere Konfiguration',
                motordiesel: 'Dieselmotor',
                motorbenzin: 'Benzin',
                motorother: 'Anderer Motortyp',
                transmissionautomatik: 'Automatikgetriebe',
                transmissionmanual: 'Schaltgetriebe',
                transmissionother: 'Andere Art von Getriebe',
                price: 'Preis',
                usedCarsOnly: 'Nur gebrauchte Autos'
            },
            carStatisticElement: {
                total: 'Total'
            },
            loading: 'Laden',
        },
    }
});
