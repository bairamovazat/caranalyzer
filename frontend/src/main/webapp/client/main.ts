import {Component, Prop, Vue} from 'vue-property-decorator';

import App from './App.vue';
import router from './router';
import store from './store';
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap/js/dist/util';
import 'bootstrap/js/dist/dropdown';
import i18n from './locale';
import FlagIcon from 'vue-flag-icon'
import moment from 'vue-moment';

Vue.use(FlagIcon);
Vue.use(BootstrapVue);
Vue.use(moment);
Vue.config.productionTip = false;

new Vue({
    i18n,
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');
