package ru.azat.caranalysis.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@Import({WebMvcConfig.class, JpaConfig.class})
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/css/**").addResourceLocations("/build/css/");
        registry.addResourceHandler("/js/**").addResourceLocations("/build/js/");
        registry.addResourceHandler("/img/**").addResourceLocations("/build/img/");
        registry.addResourceHandler("/index.html").addResourceLocations("/build/index.html");
        registry.addResourceHandler("/index.js").addResourceLocations("/build/index.js");

    }
}
