module.exports = {
    publicPath: './',
    outputDir: "src/main/webapp/build",
    // entry for the page
    pages: {
        index: {
            entry: 'src/main/webapp/client/main.ts',
            // the source template
            template: 'public/index.html',
            filename: 'index.html',
            title: 'Index Page',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'index']
        }
    }
};