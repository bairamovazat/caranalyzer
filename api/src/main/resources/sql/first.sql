Select
  F.FahrzeugID,
  F.FahrzeugHistorieIDAktuell,
  F.FahrzeugHistorieIDBewertet,
  F.FahrgestellNummer,
  F.HauptCode,
  F.FzStatusID,
  F.TreibstoffartID,
  F.FzgTypText,
  F.ModellText,
  F.ModellNr,
  F.UnterTyp,
  F.Listenneupreis,
  F.FarbCode,
  F.FarbeAussen,
  F.HerstellerID,
  F.MandantID,
  F.FarbeInnen,
  F.Erstzulassung,
  F.Serie,
  F.AktenZeichen,
  F.Schluessel,
  F.VfwZubehoer,
  F.KBAHersteller,
  F.KBAModell,
  F.COCModell,
  F.SchluesselPlatzText,
  F.AutowertVehicleID,
  Kunde.AutowertCustomerID,
  F.datECode,
  F.datContainer,
  F.datConstructionTime,
  DATDossierID,
  FH.FahrzeugHistorieID,
  FH.PolKennzeichen,
  FH.LieferDat,
  FH.VorLieferDat,
  FH.Tageszulassung,
  FH.IstMazdaMobil,
  FH.FahrzeugArtID,
  FH.FabrikatID,
  FH.HaendlerpreisBrutto,
  FH.VkPreisBrutto,
  FH.VkPreis,
  FH.EkPreis,
  FH.InternetPreis,
  FH.KmStand,
  FH.GwMitNova,
  FH.EkUeberfuehrung,
  FH.VkUeberfuehrung,
  FH.AUDatum,
  FH.HUDatum,
  FH.LetztWSDatum,
  FH.VerkaufDatum,
  FH.VorbesitzerName,
  FH.FahrzeugNr,
  FH.RGNrHersteller,
  FH.AuftragsBemerkung,
  FH.BenutzerFeld1,
  FH.BenutzerFeld2,
  FH.BenutzerFeld3,
  FH.BenutzerFeld4,
  FH.BenutzerFeld5,
  FH.BenutzerFeld6,
  FH.WE_NUMMER,
  MW.MietwagenID,
  MW.MietwagenArtID,
  MW.MietwagenAktiv,
  MW.MietwagenVon,
  MW.MietwagenBis,
  CAST( CASE
        WHEN F.FzStatusID = 0 THEN 'Modell definiert'
        WHEN F.FzStatusID = 1 THEN 'Bestellt / Vorlauf'
        WHEN F.FzStatusID = 2 THEN 'im Bestand'
        WHEN F.FzStatusID = 3 THEN 'Reserviert'
        WHEN F.FzStatusID = 4 THEN 'Verkauft / Kundenbestand'
        WHEN F.FzStatusID = 5 THEN 'Verschrottet'
        WHEN F.FzStatusID = 6 THEN 'Gestohlen'
        WHEN F.FzStatusID = 7 THEN 'Inaktiv'
        ELSE NULL
        END AS VARCHAR(50)) FzStatusText,
  CAST( CASE
        WHEN FH.FahrzeugArtID = 0 THEN 'Kundenwagen'
        WHEN FH.FahrzeugArtID = 1 THEN 'Lagerwagen'
        WHEN FH.FahrzeugArtID = 2 THEN 'Vorführwagen'
        WHEN FH.FahrzeugArtID = 3 THEN 'Dienstwagen'
        WHEN FH.FahrzeugArtID = 4 THEN 'Agenturfahrzeug'
        WHEN FH.FahrzeugArtID = 5 THEN 'Gebrauchtwagen'
        WHEN FH.FahrzeugArtID = 6 THEN 'Fremdfahrzeug'
        ELSE NULL
        END AS VARCHAR(50)) FahrzeugArtText,
  Kunde.KundeID,
  Kunde.KundenNr,
  Kunde.KaufWunschDatum,
  KADR.KurzName,
  KADR.Name1,
  KADR.Name2,
  KADR.Name3,
  KADR.Strasse,
  KADR.GeburtsDatum,
  KADR.Plz,
  KADR.Ort,
  MA.MitarbeiterNr,
  VK.Kurzname VKKurzname,
  Eink.MitarbeiterNr EinkNr,
  EinkAdr.Kurzname EinkKurzname,
  Filiale.FilialeID,
  Filiale.FilialeNr,
  HS.HerstellerName AS HSKurzName,
  SO.Standort,
  SN.SchluesselPlatzNr,
  A.AuftragID,
  A.AuftragsNr,
  A.Annahme,
  A.GewLieferdatKd,
  A.AnnehmerId,
  CASE WHEN AR.IsNKP = 1 THEN NKPBU.BelegNr ELSE BU.BelegNr END RechnungsNr,
  CASE WHEN AR.IsNKP = 1 THEN NKPBU.BelegDatum ELSE BU.BelegDatum END RechnungsDatum,
  AR.Nettobetrag,
  AR.MwstBetrag,
  AR.BruttoBetrag,
  CAST( CASE
        WHEN FH.LieferDat is NULL THEN NULL
        WHEN FH.Geschaeftswagen = 1 THEN NULL
        WHEN F.FzStatusID <= 2 THEN
          CAST('TODAY' as DATE) - CAST(FH.LieferDat as DATE)
        WHEN NOT IIF(AR.IsNKP = 1,
                     NKPBU.BelegDatum,
                     BU.BelegDatum)  is NULL THEN
          CAST(IIF(AR.IsNKP = 1,
                   NKPBU.BelegDatum,
                   BU.BelegDatum) as DATE) - CAST(FH.LieferDat as DATE)
        WHEN NOT FH.VerkaufDatum is NULL THEN
          CAST(FH.VerkaufDatum as DATE) - CAST(FH.LieferDat as DATE)
        ELSE
          CAST('TODAY' as DATE) - CAST(FH.LieferDat as DATE)
        END As Integer) StandTage,
  (SELECT K.FzKreditzuordnungID   FROM FzKreditzuordnung K   WHERE K.FahrzeughistorieID = FH.FahrzeugHistorieID    AND K.Vorzeitig = 0    AND K.Neueroeffnet = 0    AND (SELECT SUM(D.SollBetrag - D.IstBetrag)        FROM FzKreditDauer D        WHERE D.FzKreditzuordnungID = K.FzKreditzuordnungID) > 0) FzKreditzuordnungID ,
  CASE WHEN F.HerstellerID IN (13,
                               22) THEN CAST(FH.BestellNummer AS VARCHAR(15)) ELSE CAST(FH.MvisNummer AS VARCHAR(15)) END AS BestellNr
from Fahrzeug F
  LEFT JOIN FahrzeugHistorie FH ON FH.FahrzeugHistorieID = F.FahrzeugHistorieIDAktuell
  LEFT JOIN Kunde ON Kunde.KundeID = FH.KundeID
  LEFT JOIN Adresse KADR ON KADR.AdresseID = Kunde.AdresseID
  LEFT JOIN Filiale ON Filiale.FilialeID = F.FilialeID
  LEFT JOIN Hersteller HS ON HS.HerstellerID = F.HerstellerID
  LEFT JOIN StandOrt SO ON SO.StandOrtID = FH.StandOrtID
  LEFT JOIN AutoSchluesselPlatz SN ON SN.AutoSchluesselPlatzID = F.AutoSchluesselPlatzID
  LEFT JOIN Auftrag A ON A.AuftragID = FH.AuftragID
  LEFT JOIN AuftragRechnung AR ON AR.AuftragID = FH.AuftragID
  LEFT JOIN Buchung BU ON BU.BuchungID = AR.BuchungID
  LEFT JOIN NKPBuchung NKPBU ON NKPBU.NKPBuchungID = AR.NKPBuchungID
  LEFT JOIN Mitarbeiter MA ON MA.MitarbeiterID = FH.VerkaeuferID
  LEFT JOIN Adresse VK ON VK.AdresseID = MA.AdresseID
  LEFT JOIN Mitarbeiter Eink ON Eink.MitarbeiterID = FH.EinkaeuferGWID
  LEFT JOIN Adresse EinkAdr ON EinkAdr.AdresseID = Eink.AdresseID
  LEFT JOIN Mietwagen MW ON MW.FahrzeugID = F.FahrzeugID
WHERE
  ( F.MandantId =  1)
ORDER BY 4;

Select
  F.FahrzeugID,
  F.FahrzeugHistorieIDAktuell,
  F.FahrzeugHistorieIDBewertet,
  F.FahrgestellNummer,
  F.HauptCode,
  F.FzStatusID,
  F.TreibstoffartID,
  F.FzgTypText,
  F.ModellText,
  F.ModellNr,
  F.UnterTyp,
  F.Listenneupreis,
  F.FarbCode,
  F.FarbeAussen,
  F.HerstellerID,
  F.MandantID,
  F.FarbeInnen,
  F.Erstzulassung,
  F.Serie,
  F.AktenZeichen,
  F.Schluessel,
  F.VfwZubehoer,
  F.KBAHersteller,
  F.KBAModell,
  F.COCModell,
  F.SchluesselPlatzText,
  F.AutowertVehicleID,
  Kunde.AutowertCustomerID,
  F.datECode,
  F.datContainer,
  F.datConstructionTime,
  DATDossierID,
  FH.FahrzeugHistorieID,
  FH.PolKennzeichen,
  FH.LieferDat,
  FH.VorLieferDat,
  FH.Tageszulassung,
  FH.IstMazdaMobil,
  FH.FahrzeugArtID,
  FH.FabrikatID,
  FH.HaendlerpreisBrutto,
  FH.VkPreisBrutto,
  FH.VkPreis,
  FH.EkPreis,
  FH.InternetPreis,
  FH.KmStand,
  FH.GwMitNova,
  FH.EkUeberfuehrung,
  FH.VkUeberfuehrung,
  FH.AUDatum,
  FH.HUDatum,
  FH.LetztWSDatum,
  FH.VerkaufDatum,
  FH.VorbesitzerName,
  FH.FahrzeugNr,
  FH.RGNrHersteller,
  FH.AuftragsBemerkung,
  FH.BenutzerFeld1,
  FH.BenutzerFeld2,
  FH.BenutzerFeld3,
  FH.BenutzerFeld4,
  FH.BenutzerFeld5,
  FH.BenutzerFeld6,
  FH.WE_NUMMER,
  MW.MietwagenID,
  MW.MietwagenArtID,
  MW.MietwagenAktiv,
  MW.MietwagenVon,
  MW.MietwagenBis,
  CAST( CASE
        WHEN F.FzStatusID = 0 THEN 'Modell definiert'
        WHEN F.FzStatusID = 1 THEN 'Bestellt / Vorlauf'
        WHEN F.FzStatusID = 2 THEN 'im Bestand'
        WHEN F.FzStatusID = 3 THEN 'Reserviert'
        WHEN F.FzStatusID = 4 THEN 'Verkauft / Kundenbestand'
        WHEN F.FzStatusID = 5 THEN 'Verschrottet'
        WHEN F.FzStatusID = 6 THEN 'Gestohlen'
        WHEN F.FzStatusID = 7 THEN 'Inaktiv'
        ELSE NULL
        END AS VARCHAR(50)) FzStatusText,
  CAST( CASE
        WHEN FH.FahrzeugArtID = 0 THEN 'Kundenwagen'
        WHEN FH.FahrzeugArtID = 1 THEN 'Lagerwagen'
        WHEN FH.FahrzeugArtID = 2 THEN 'Vorführwagen'
        WHEN FH.FahrzeugArtID = 3 THEN 'Dienstwagen'
        WHEN FH.FahrzeugArtID = 4 THEN 'Agenturfahrzeug'
        WHEN FH.FahrzeugArtID = 5 THEN 'Gebrauchtwagen'
        WHEN FH.FahrzeugArtID = 6 THEN 'Fremdfahrzeug'
        ELSE NULL
        END AS VARCHAR(50)) FahrzeugArtText,
  Kunde.KundeID,
  Kunde.KundenNr,
  Kunde.KaufWunschDatum,
  KADR.KurzName,
  KADR.Name1,
  KADR.Name2,
  KADR.Name3,
  KADR.Strasse,
  KADR.GeburtsDatum,
  KADR.Plz,
  KADR.Ort,
  MA.MitarbeiterNr,
  VK.Kurzname VKKurzname,
  Eink.MitarbeiterNr EinkNr,
  EinkAdr.Kurzname EinkKurzname,
  Filiale.FilialeID,
  Filiale.FilialeNr,
  HS.HerstellerName AS HSKurzName,
  SO.Standort,
  SN.SchluesselPlatzNr,
  A.AuftragID,
  A.AuftragsNr,
  A.Annahme,
  A.GewLieferdatKd,
  A.AnnehmerId,
  CASE WHEN AR.IsNKP = 1 THEN NKPBU.BelegNr ELSE BU.BelegNr END RechnungsNr,
  CASE WHEN AR.IsNKP = 1 THEN NKPBU.BelegDatum ELSE BU.BelegDatum END RechnungsDatum,
  AR.Nettobetrag,
  AR.MwstBetrag,
  AR.BruttoBetrag,
  CAST( CASE
        WHEN FH.LieferDat is NULL THEN NULL
        WHEN FH.Geschaeftswagen = 1 THEN NULL
        WHEN F.FzStatusID <= 2 THEN
          CAST('TODAY' as DATE) - CAST(FH.LieferDat as DATE)
        WHEN NOT IIF(AR.IsNKP = 1,
                     NKPBU.BelegDatum,
                     BU.BelegDatum)  is NULL THEN
          CAST(IIF(AR.IsNKP = 1,
                   NKPBU.BelegDatum,
                   BU.BelegDatum) as DATE) - CAST(FH.LieferDat as DATE)
        WHEN NOT FH.VerkaufDatum is NULL THEN
          CAST(FH.VerkaufDatum as DATE) - CAST(FH.LieferDat as DATE)
        ELSE
          CAST('TODAY' as DATE) - CAST(FH.LieferDat as DATE)
        END As Integer) StandTage,
  (SELECT K.FzKreditzuordnungID   FROM FzKreditzuordnung K   WHERE K.FahrzeughistorieID = FH.FahrzeugHistorieID    AND K.Vorzeitig = 0    AND K.Neueroeffnet = 0    AND (SELECT SUM(D.SollBetrag - D.IstBetrag)        FROM FzKreditDauer D        WHERE D.FzKreditzuordnungID = K.FzKreditzuordnungID) > 0) FzKreditzuordnungID ,
  CASE WHEN F.HerstellerID IN (13,
                               22) THEN CAST(FH.BestellNummer AS VARCHAR(15)) ELSE CAST(FH.MvisNummer AS VARCHAR(15)) END AS BestellNr
from Fahrzeug F
  LEFT JOIN FahrzeugHistorie FH ON FH.FahrzeugHistorieID = F.FahrzeugHistorieIDAktuell
  LEFT JOIN Kunde ON Kunde.KundeID = FH.KundeID
  LEFT JOIN Adresse KADR ON KADR.AdresseID = Kunde.AdresseID
  LEFT JOIN Filiale ON Filiale.FilialeID = F.FilialeID
  LEFT JOIN Hersteller HS ON HS.HerstellerID = F.HerstellerID
  LEFT JOIN StandOrt SO ON SO.StandOrtID = FH.StandOrtID
  LEFT JOIN AutoSchluesselPlatz SN ON SN.AutoSchluesselPlatzID = F.AutoSchluesselPlatzID
  LEFT JOIN Auftrag A ON A.AuftragID = FH.AuftragID
  LEFT JOIN AuftragRechnung AR ON AR.AuftragID = FH.AuftragID
  LEFT JOIN Buchung BU ON BU.BuchungID = AR.BuchungID
  LEFT JOIN NKPBuchung NKPBU ON NKPBU.NKPBuchungID = AR.NKPBuchungID
  LEFT JOIN Mitarbeiter MA ON MA.MitarbeiterID = FH.VerkaeuferID
  LEFT JOIN Adresse VK ON VK.AdresseID = MA.AdresseID
  LEFT JOIN Mitarbeiter Eink ON Eink.MitarbeiterID = FH.EinkaeuferGWID
  LEFT JOIN Adresse EinkAdr ON EinkAdr.AdresseID = Eink.AdresseID
  LEFT JOIN Mietwagen MW ON MW.FahrzeugID = F.FahrzeugID
WHERE
  ( F.MandantId =  1
    and FH.FahrzeugArtId in (5)
    and F.FzStatusId in (1)  )
Order By 54;

Select
  F.FahrzeugID,
  F.FahrzeugHistorieIDAktuell,
  F.FahrzeugHistorieIDBewertet,
  F.FahrgestellNummer,
  F.HauptCode,
  F.FzStatusID,
  F.TreibstoffartID,
  F.FzgTypText,
  F.ModellText,
  F.ModellNr,
  F.UnterTyp,
  F.Listenneupreis,
  F.FarbCode,
  F.FarbeAussen,
  F.HerstellerID,
  F.MandantID,
  F.FarbeInnen,
  F.Erstzulassung,
  F.Serie,
  F.AktenZeichen,
  F.Schluessel,
  F.VfwZubehoer,
  F.KBAHersteller,
  F.KBAModell,
  F.COCModell,
  F.SchluesselPlatzText,
  F.AutowertVehicleID,
  Kunde.AutowertCustomerID,
  F.datECode,
  F.datContainer,
  F.datConstructionTime,
  DATDossierID,
  FH.FahrzeugHistorieID,
  FH.PolKennzeichen,
  FH.LieferDat,
  FH.VorLieferDat,
  FH.Tageszulassung,
  FH.IstMazdaMobil,
  FH.FahrzeugArtID,
  FH.FabrikatID,
  FH.HaendlerpreisBrutto,
  FH.VkPreisBrutto,
  FH.VkPreis,
  FH.EkPreis,
  FH.InternetPreis,
  FH.KmStand,
  FH.GwMitNova,
  FH.EkUeberfuehrung,
  FH.VkUeberfuehrung,
  FH.AUDatum,
  FH.HUDatum,
  FH.LetztWSDatum,
  FH.VerkaufDatum,
  FH.VorbesitzerName,
  FH.FahrzeugNr,
  FH.RGNrHersteller,
  FH.AuftragsBemerkung,
  FH.BenutzerFeld1,
  FH.BenutzerFeld2,
  FH.BenutzerFeld3,
  FH.BenutzerFeld4,
  FH.BenutzerFeld5,
  FH.BenutzerFeld6,
  FH.WE_NUMMER,
  MW.MietwagenID,
  MW.MietwagenArtID,
  MW.MietwagenAktiv,
  MW.MietwagenVon,
  MW.MietwagenBis,
  CAST( CASE
        WHEN F.FzStatusID = 0 THEN 'Modell definiert'
        WHEN F.FzStatusID = 1 THEN 'Bestellt / Vorlauf'
        WHEN F.FzStatusID = 2 THEN 'im Bestand'
        WHEN F.FzStatusID = 3 THEN 'Reserviert'
        WHEN F.FzStatusID = 4 THEN 'Verkauft / Kundenbestand'
        WHEN F.FzStatusID = 5 THEN 'Verschrottet'
        WHEN F.FzStatusID = 6 THEN 'Gestohlen'
        WHEN F.FzStatusID = 7 THEN 'Inaktiv'
        ELSE NULL
        END AS VARCHAR(50)) FzStatusText,
  CAST( CASE
        WHEN FH.FahrzeugArtID = 0 THEN 'Kundenwagen'
        WHEN FH.FahrzeugArtID = 1 THEN 'Lagerwagen'
        WHEN FH.FahrzeugArtID = 2 THEN 'Vorführwagen'
        WHEN FH.FahrzeugArtID = 3 THEN 'Dienstwagen'
        WHEN FH.FahrzeugArtID = 4 THEN 'Agenturfahrzeug'
        WHEN FH.FahrzeugArtID = 5 THEN 'Gebrauchtwagen'
        WHEN FH.FahrzeugArtID = 6 THEN 'Fremdfahrzeug'
        ELSE NULL
        END AS VARCHAR(50)) FahrzeugArtText,
  Kunde.KundeID,
  Kunde.KundenNr,
  Kunde.KaufWunschDatum,
  KADR.KurzName,
  KADR.Name1,
  KADR.Name2,
  KADR.Name3,
  KADR.Strasse,
  KADR.GeburtsDatum,
  KADR.Plz,
  KADR.Ort,
  MA.MitarbeiterNr,
  VK.Kurzname VKKurzname,
  Eink.MitarbeiterNr EinkNr,
  EinkAdr.Kurzname EinkKurzname,
  Filiale.FilialeID,
  Filiale.FilialeNr,
  HS.HerstellerName AS HSKurzName,
  SO.Standort,
  SN.SchluesselPlatzNr,
  A.AuftragID,
  A.AuftragsNr,
  A.Annahme,
  A.GewLieferdatKd,
  A.AnnehmerId,
  CASE WHEN AR.IsNKP = 1 THEN NKPBU.BelegNr ELSE BU.BelegNr END RechnungsNr,
  CASE WHEN AR.IsNKP = 1 THEN NKPBU.BelegDatum ELSE BU.BelegDatum END RechnungsDatum,
  AR.Nettobetrag,
  AR.MwstBetrag,
  AR.BruttoBetrag,
  CAST( CASE
        WHEN FH.LieferDat is NULL THEN NULL
        WHEN FH.Geschaeftswagen = 1 THEN NULL
        WHEN F.FzStatusID <= 2 THEN
          CAST('TODAY' as DATE) - CAST(FH.LieferDat as DATE)
        WHEN NOT IIF(AR.IsNKP = 1,
                     NKPBU.BelegDatum,
                     BU.BelegDatum)  is NULL THEN
          CAST(IIF(AR.IsNKP = 1,
                   NKPBU.BelegDatum,
                   BU.BelegDatum) as DATE) - CAST(FH.LieferDat as DATE)
        WHEN NOT FH.VerkaufDatum is NULL THEN
          CAST(FH.VerkaufDatum as DATE) - CAST(FH.LieferDat as DATE)
        ELSE
          CAST('TODAY' as DATE) - CAST(FH.LieferDat as DATE)
        END As Integer) StandTage,
  (SELECT K.FzKreditzuordnungID   FROM FzKreditzuordnung K   WHERE K.FahrzeughistorieID = FH.FahrzeugHistorieID    AND K.Vorzeitig = 0    AND K.Neueroeffnet = 0    AND (SELECT SUM(D.SollBetrag - D.IstBetrag)        FROM FzKreditDauer D        WHERE D.FzKreditzuordnungID = K.FzKreditzuordnungID) > 0) FzKreditzuordnungID ,
  CASE WHEN F.HerstellerID IN (13,
                               22) THEN CAST(FH.BestellNummer AS VARCHAR(15)) ELSE CAST(FH.MvisNummer AS VARCHAR(15)) END AS BestellNr
from Fahrzeug F
  LEFT JOIN FahrzeugHistorie FH ON FH.FahrzeugHistorieID = F.FahrzeugHistorieIDAktuell
  LEFT JOIN Kunde ON Kunde.KundeID = FH.KundeID
  LEFT JOIN Adresse KADR ON KADR.AdresseID = Kunde.AdresseID
  LEFT JOIN Filiale ON Filiale.FilialeID = F.FilialeID
  LEFT JOIN Hersteller HS ON HS.HerstellerID = F.HerstellerID
  LEFT JOIN StandOrt SO ON SO.StandOrtID = FH.StandOrtID
  LEFT JOIN AutoSchluesselPlatz SN ON SN.AutoSchluesselPlatzID = F.AutoSchluesselPlatzID
  LEFT JOIN Auftrag A ON A.AuftragID = FH.AuftragID
  LEFT JOIN AuftragRechnung AR ON AR.AuftragID = FH.AuftragID
  LEFT JOIN Buchung BU ON BU.BuchungID = AR.BuchungID
  LEFT JOIN NKPBuchung NKPBU ON NKPBU.NKPBuchungID = AR.NKPBuchungID
  LEFT JOIN Mitarbeiter MA ON MA.MitarbeiterID = FH.VerkaeuferID
  LEFT JOIN Adresse VK ON VK.AdresseID = MA.AdresseID
  LEFT JOIN Mitarbeiter Eink ON Eink.MitarbeiterID = FH.EinkaeuferGWID
  LEFT JOIN Adresse EinkAdr ON EinkAdr.AdresseID = Eink.AdresseID
  LEFT JOIN Mietwagen MW ON MW.FahrzeugID = F.FahrzeugID
WHERE
  ( F.MandantId =  1
    and FH.FahrzeugArtId in (5)
    and F.FzStatusId in (4)  )
ORDER BY 4;
