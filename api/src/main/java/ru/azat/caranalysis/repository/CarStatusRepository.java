package ru.azat.caranalysis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.azat.caranalysis.model.CarStatus;

import java.util.List;
import java.util.Optional;

@Repository
public interface CarStatusRepository extends JpaRepository<CarStatus, Long>{

    List<CarStatus> findAllByStatusLike(String status);

}
