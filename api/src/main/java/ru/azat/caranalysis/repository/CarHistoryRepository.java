package ru.azat.caranalysis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.azat.caranalysis.model.CarHistory;

@Repository
public interface CarHistoryRepository extends JpaRepository<CarHistory, Long> {
}
