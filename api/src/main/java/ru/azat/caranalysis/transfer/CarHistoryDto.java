package ru.azat.caranalysis.transfer;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.azat.caranalysis.model.CarHistory;
import ru.azat.caranalysis.model.CarStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CarHistoryDto {

    private Long id;

    private String mark;

    private Long carType;

    private Double price;

    @JsonFormat(pattern = "yyy-MM-dd hh:mm:ss")
    private Date deliveryDate;

    public static CarHistoryDto from(CarHistory carHistory) {
        return CarHistoryDto.builder()
                .id(carHistory.getId())
                .mark(carHistory.getMark())
                .price(carHistory.getPrice())
                .carType(carHistory.getCarType())
                .deliveryDate(carHistory.getDeliveryDate())
                .build();
    }

    public static List<CarHistoryDto> from(List<CarHistory> carHistoryList) {
        return carHistoryList.stream()
                .map(CarHistoryDto::from)
                .collect(Collectors.toList());
    }
}
