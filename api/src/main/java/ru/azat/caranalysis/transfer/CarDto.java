package ru.azat.caranalysis.transfer;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.azat.caranalysis.model.Car;
import ru.azat.caranalysis.model.CarStatus;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CarDto {
    public Long id;
    public String model;
    public CarStatusDto carStatus;
    public CarHistoryDto carHistory;

    public static CarDto from(Car car){
        return CarDto.builder()
                .id(car.getId())
                .model(car.getModel())
                .carStatus(CarStatusDto.from(car.getStatus(), false))
                .carHistory(car.getCarHistory() == null ? null : CarHistoryDto.from(car.getCarHistory()))
                .build();
    }

    public static List<CarDto> from(List<Car> cars){
        return cars.stream().map(CarDto::from).collect(Collectors.toList());
    }
}
