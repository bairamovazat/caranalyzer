package ru.azat.caranalysis.transfer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.azat.caranalysis.model.CarStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CarStatusDto {
    private Long id;

    private String status;

    private List<CarDto> cars;

    public static CarStatusDto from(CarStatus carStatus, boolean withCars) {
        return CarStatusDto.builder()
                .id(carStatus.getId())
                .status(carStatus.getStatus())
                .cars(withCars ? CarDto.from(carStatus.getCars()) : new ArrayList<>())
                .build();
    }

    public static List<CarStatusDto> from(List<CarStatus> carStatusList, boolean withCars) {
        return carStatusList.stream()
                .map(s -> from(s, withCars))
                .collect(Collectors.toList());
    }
}
