package ru.azat.caranalysis.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"ru.azat.caranalysis.repository"})
@EntityScan(basePackages = "ru.azat.caranalysis.model")
public class JpaConfig {
}
