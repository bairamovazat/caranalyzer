package ru.azat.caranalysis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.azat.caranalysis.model.CarHistory;
import ru.azat.caranalysis.repository.CarHistoryRepository;

import java.util.List;

@Service
public class CarHistoryServiceImpl implements CarHistoryService {

    @Autowired
    private CarHistoryRepository carHistoryRepository;

    @Override
    public List<CarHistory> findAll(){
        return carHistoryRepository.findAll();
    }

}
