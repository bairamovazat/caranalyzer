package ru.azat.caranalysis.service;

import ru.azat.caranalysis.model.CarHistory;

import java.util.List;

public interface CarHistoryService {
    List<CarHistory> findAll();
}
