package ru.azat.caranalysis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.azat.caranalysis.model.Car;
import ru.azat.caranalysis.model.CarStatus;
import ru.azat.caranalysis.repository.CarStatusRepository;
import ru.azat.caranalysis.transfer.CarDto;
import ru.azat.caranalysis.transfer.CarStatusDto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CarStatusServiceImpl implements CarStatusService {

    @Autowired
    private CarStatusRepository carStatusRepository;

    @Override
    public List<CarStatusDto> findAll(){
        return CarStatusDto.from(carStatusRepository.findAll(), false);
    }

    @Override
    public List<CarStatusDto> getByNameLike(String status) {
        return CarStatusDto.from(carStatusRepository.findAllByStatusLike(status), false);
    }

    @Override
    public CarStatusDto getById(Long id) {
        return CarStatusDto.from(
                carStatusRepository.findById(id)
                        .orElseThrow(() -> new IllegalArgumentException("Status not found")),
                true
        );
    }

    @Override
    public List<CarDto> getStatusCars(Long id) {
        return CarDto.from(
                carStatusRepository.findById(id)
                        .orElseThrow(() -> new IllegalArgumentException("Status not found")).getCars()
        );
    }

    @Override
    public List<CarDto> getStatusCarsByCarType(Long id, Long carType) {
//        List<Car> cars = carStatusRepository.findById(id)
//                .orElseThrow(() -> new IllegalArgumentException("Status not found")).getCars()
//                .stream()
//                .filter(car -> (car.getCarHistory() != null && Objects.equals(car.getCarHistory().getCarType(), carType)))
//                .filter(car -> {
//                    try {
//                        return car.getCarHistory().getDeliveryDate() != null && car.getCarHistory().getDeliveryDate().after(new SimpleDateFormat("dd/MM/yyyy").parse("01/06/2019"));
//                    } catch (ParseException e) {
//                        return true;
//                    }
//                })
//                .collect(Collectors.toList());
//
//        int i = 0;
        return CarDto.from(
                carStatusRepository.findById(id)
                        .orElseThrow(() -> new IllegalArgumentException("Status not found")).getCars()
                .stream()
                .filter(car -> (car.getCarHistory() != null && Objects.equals(car.getCarHistory().getCarType(), carType)))
                .collect(Collectors.toList())
        );
    }
}
