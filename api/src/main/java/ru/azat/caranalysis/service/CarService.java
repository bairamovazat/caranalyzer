package ru.azat.caranalysis.service;

import ru.azat.caranalysis.transfer.CarDto;

import java.util.List;

public interface CarService {

    CarDto getCar(Long id);
}
