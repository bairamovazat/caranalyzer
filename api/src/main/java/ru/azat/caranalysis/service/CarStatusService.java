package ru.azat.caranalysis.service;

import ru.azat.caranalysis.model.Car;
import ru.azat.caranalysis.model.CarStatus;
import ru.azat.caranalysis.transfer.CarDto;
import ru.azat.caranalysis.transfer.CarStatusDto;

import java.util.List;

public interface CarStatusService {
    List<CarStatusDto> findAll();

    List<CarStatusDto> getByNameLike(String status);

    CarStatusDto getById(Long id);

    List<CarDto> getStatusCars(Long id);

    List<CarDto> getStatusCarsByCarType(Long id, Long carType);

}
