package ru.azat.caranalysis.service;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.azat.caranalysis.model.Car;
import ru.azat.caranalysis.repository.CarRepository;
import ru.azat.caranalysis.transfer.CarDto;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarRepository carRepository;

    @Override
    public CarDto getCar(Long id) {
        return CarDto.from(
                carRepository.findById(id)
                        .orElseThrow(
                                () -> new IllegalArgumentException("Car not found")
                        )
        );
    }
}
