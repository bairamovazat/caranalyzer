package ru.azat.caranalysis.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
public class MazdaController {

    @GetMapping("/balance")
    public Object getCurrentScore(){
        return 863790.45;
    }
}
