package ru.azat.caranalysis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.azat.caranalysis.service.CarService;
import ru.azat.caranalysis.transfer.CarDto;

import java.util.List;

@RestController
@RequestMapping("/car")
public class CarController {
    @Autowired
    private CarService carService;

    @GetMapping("/{id}")
    private CarDto getCar(@PathVariable("id")Long id){
        return carService.getCar(id);
    }
}
