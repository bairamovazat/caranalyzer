package ru.azat.caranalysis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.azat.caranalysis.service.CarStatusService;
import ru.azat.caranalysis.transfer.CarDto;
import ru.azat.caranalysis.transfer.CarStatusDto;

import java.util.List;

@RestController
@RequestMapping("/statuses")
public class StatusController {

    @Autowired
    private CarStatusService carStatusService;

    @GetMapping("")
    public List<CarStatusDto> getByStatus(@RequestParam(value = "statusLike", required = false) String status) {
        if(status == null){
            return carStatusService.findAll();
        }
        return carStatusService.getByNameLike(status);
    }

    @GetMapping("/{id}")
    public CarStatusDto getById(@PathVariable(value = "id", required = false) Long id) {
        return carStatusService.getById(id);
    }

    @GetMapping("/{id}/cars")
    public List<CarDto> getCarsById(@PathVariable(value = "id", required = false) Long id,
                                    @RequestParam(value = "carType", required = false) Long carType) {
        if(carType != null){
            return carStatusService.getStatusCarsByCarType(id, carType);
        }
        return carStatusService.getStatusCars(id);
    }
}
