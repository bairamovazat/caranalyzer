package ru.azat.caranalysis.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "FAHRZEUG")
public class Car {

    @Id
    @Column(name = "FAHRZEUGID")
    public Long id;

    @Column(name = "MODELLTEXT", length = 80)
    public String model;

    @ManyToOne
    @JoinColumn(name = "FZSTATUSID")
    private CarStatus status;

    @Column(name = "COCMOTORARTKURZ")
    private String engineType;

//    @Column(name = "LISTENNEUPREIS")
//    private Double price;

    @ManyToOne
    @JoinColumn(name = "FAHRZEUGHISTORIEIDAKTUELL")
    @NotFound(action = NotFoundAction.IGNORE)
    private CarHistory carHistory;

    //CX5, CX3 и тд
    @Column(name = "SERIE")
    private String series;

}
