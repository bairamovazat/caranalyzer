package ru.azat.caranalysis.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "FAHRZEUGHISTORIE")
@DynamicInsert
@DynamicUpdate
public class CarHistory {

    @Id
    @Column(name = "FAHRZEUGHISTORIEID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "STANDORTID")
    private HistoryPlace historyPlace;

    @Column(name = "VORBESITZERNAME")
    private String mark;

    @Column(name = "FAHRZEUGARTID")
    private Long carType;

    @Column(name = "LIEFERDAT")
    private Date deliveryDate;

    @Column(name = "EKPREIS")
    private Double price;
}
