package ru.azat.caranalysis.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "STANDORT")
public class HistoryPlace {

    @Id
    @Column(name = "STANDORTID")
    private Long id;

    @Column(name = "STANDORT")
    private String place;
}
