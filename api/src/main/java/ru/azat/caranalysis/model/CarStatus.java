package ru.azat.caranalysis.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "FZSTATUS")
public class CarStatus {

    @Id
    @Column(name = "FZSTATUSID")
    private Long id;

    @Column(name = "FZSTATUSTEXT")
    private String status;

    @OneToMany(mappedBy = "status")
    private List<Car> cars;
}
